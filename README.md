# processJPG

`processJPG` is a shellscript to *losslessly* apply some awesome modifications to image files.

### Image Rotation
Cameras normally store the picture relative to the direction of the image sensor -- so always horizontally.
If the camera has a gravitational sensor to measure the direction of the camera, it is stored the same way but the image will get a flag, telling the viewer to rotate the image while displaying.
This is faster for the camera since it does not need to rotate the image itself.
For compatibility reasons though it makes sense to rotate the actual image according to the flag in the file and remove the flag afterwars.

`jhead` does this without any loss in quality.

### File Name
To be able to merge the files of multiple cameras (e.g. from the same wedding) it makes total sense to normalize the filenames and include the timestamp of the recording time.
This way, the pictures can be viewed in order in the file browser.

The files will be named
```
YYYY-MM-DD_HH-MM-SS[_c].jpg
```
The optional `_c` is an automatically added counter, if another picture taken at the same second already exists. (..._1.jpg, ..._2.jpg, ...)

Cameras normally store the recording time in the exif information of the picture. The only requirement is, that the camera's time is set correctly.

### File Size
In the last step, the file size will be optimized. Depending on the processing of the camera that took the picture, this can lead to a decrease of filesize by a two digit percentage.
These savings are achieved by mathematically optimizing some compression matrices.
It just takes some time (depending on your setup, roughly 1s per image). This is also the reason cameras don't do that so well directly -- they want to find a sweet spot between quickly saving the image and reducing the filesize. Another reason might be poorly optimizing jpeg-libraries.

The shrinkage is recommended before archiving and backing up an image since you will save a lot of disk space.

This is also a lossless operation. 

## Installation and Usage

### Install the prerequisites
* jhead
* exiftool
* jpegoptim

On Debian 10 you can use the following command:
```
# apt-get install libimage-exiftool-perl jhead jpegoptim
```

For arch based distros, use:
```
# pacman -S perl-image-exiftool jhead jpegoptim
```

### Install the script
```
# curl -s https://codeberg.org/hanser/processJPG/raw/branch/master/processJPG | tee /usr/local/bin/processJPG
# chmod +x /usr/local/bin/processJPG
```

### Usage
Please make a backup of your files *before* running the script, in case something goes wrong! Don't blame me afterwards.

To run the three operations on the jpeg files in the current directory run 
```
$ processJPG
```
Or add the `-r` parameter to run the optimizations recursively on all subfolders of the current directory as well.
```
$ processJPG -r
```

## Installation via Docker

```
docker run -d --name processjpg -v /path/to/image/folder:/srv/media/ hansers/processjpg
```
This will run the script immediately on the provided folder on container creation as well as every night at 3am via a cron job.

## License

[MIT License](https://codeberg.org/hanser/processJPG/raw/branch/master/LICENSE)

