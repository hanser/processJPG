FROM debian:buster-slim

RUN apt-get -y update && DEBIAN_FRONTEND=noninteractive apt-get install -y libimage-exiftool-perl jhead jpegoptim cron --no-install-recommends && rm -rf /var/lib/apt/lists/*

ADD crontab /etc/cron.d/processjpg-cron
RUN chmod 0644 /etc/cron.d/processjpg-cron
RUN touch /var/log/cron.log

COPY processJPG /srv

VOLUME /srv/media

WORKDIR /srv

CMD bash processJPG -r && chown -R `stat -c "%u:%g" /srv/media` /srv/media/* && cron -f
